-- Update Magento store URLs and cookie domains
REPLACE INTO `core_config_data` (`path`, `value`, `scope`, `scope_id`)
VALUES
     -- Base URLs
    ('web/unsecure/base_url', 'https://www.hankypanky.com/', 'default', 0),
    ('web/secure/base_url', 'https://www.hankypanky.com/', 'default', 0),
    ('web/cookie/cookie_domain', 'www.hankypanky.com', 'default', 0),
    ('web/unsecure/base_url', 'https://admin.hankypanky.com/', 'stores', 0),
    ('web/secure/base_url', 'https://admin.hankypanky.com/', 'stores', 0),
    ('web/cookie/cookie_domain', 'admin.hankypanky.com', 'stores', 0)
;
